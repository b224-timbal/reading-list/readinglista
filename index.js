// console.log("Hello World!");

// 1.Write a JavaScript function that returns a passed string with letters in alphabetical order.
function stringSorter(string){
	console.log(string)
	string = string.split('');
	string.sort();
	string = string.join("");
	console.log(string);

};


stringSorter("webmaster");

console.log("\n\n\n");

// 2. Write a JavaScript function that accepts a string as a parameter and counts the number of vowels within the string.

function vowelCounter(word){
	console.log(word);
	let count = 0;
	for(let x = 0; x < word.length; x++){
		if (
				word[x].toLowerCase() == "a" ||
				word[x].toLowerCase() == "e" ||
				word[x].toLowerCase() == "i" ||
				word[x].toLowerCase() == "o" ||
				word[x].toLowerCase() == "u" 

			) {
			count ++;
		
		} else {
			continue;
		}

		
	}
	console.log(`Vowel Count is ${count}`);

};

vowelCounter("The quick brown fox.");

console.log("\n\n\n");

//3. Write a javaScript function that inserts new elements at the beginning of the array and sorts them in alphabetical order. Add the rest of the member countries of South East Asia.
let countries = ["Philippines", "Indonesia", "Vietnam" ];
	
function insertCountry(country) {
	console.log(`Inserted country: ${country}`);
	countries.unshift(country);
	countries.sort();
	console.log(countries)

};

insertCountry("Thailand");

console.log("\n\n\n");

//4.Using object literals, create a person object with the following properties: firstname, lastName, age, gender, nationality. Print the object in the console.

let person = {
	firstName: "John",
	lastName: "Wick",
	age: 58,
	gender: "Male",
	nationality: "Canadian"
};

console.log(person);

console.log("\n\n\n");

//5. Using constructor function, create 3 objects with the same structure. These 3 objects must have at least 5 key-value pairs, and 1 of the property must be a method that allows the object to print a message in console.

function Hero(name, level) {
	this.name = name
	this.level = level
	this.health = level*5
	this.attack = level * 3

	this.shoot = function(target) {
		console.log(`${this.name} shoots ${target.name} using handgun!\n\n`);
		console.log("\n\n\n");
		console.log(`Bang! Bang! Bang!\n\n`);
		console.log("\n\n\n");
		console.log(`${target.name}: AAAAAARGHHH!!!\n\n`);
		console.log("\n\n\n");
		target.health = target.health -this.attack
		console.log(`${target.name}'s health is now reduced to ${target.health}!`);
		console.log("\n\n\n");
		if(target.health <= 0 ) {
			console.log(`${target.name}: "This is not over!"`);
			console.log("\n\n\n");
			console.log(`${target.name} is dead..`);
			console.log("\n\n\n");
			console.log(`${this.name}: "Whoever Comes, Whoever It Is, I'll Kill Them! I'll Kill Them All!"\n\n`)
			console.log("\n\n\n");
			console.log("THE END")
		} else {
			console.log(`${target.name} shoot's back!!! `)
			console.log("\n\n\n");
			this.health = this.health - target.attack;
			console.log(`${this.name}'s health is now reduced to ${this.health}!`);
			console.log("\n\n\n");

			if (this.health <= 0 ){
				this.dead();
			} else {
				john.shoot(viggo);
			}
		}
		



	}

	this.dead = function() {
			console.log(`${this.name}: "This is not over!"`);
			console.log("\n\n\n");
			console.log(`${this.name} is dead..`);
			console.log("\n\n\n");
			
	}


};

let john = new Hero("John Wick", 20);
let viggo = new Hero("Viggo", 20);

console.log(john);
console.log(viggo);

console.log("\n\n\n");

john.shoot(viggo);